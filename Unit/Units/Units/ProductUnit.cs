﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unit.Units
{
    public abstract class ProductUnit
    {    
        public abstract int Update(long ticks);
    }
}
